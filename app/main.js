(function(){
  'use strict';
  
  angular
    .module("solase", ['ngAnimate', 'ngRoute', 'ngCookies', 'solaseServices', 'solaseControllers', 'solaseDirectives', 'angularModalService', 'angularify.semantic.sidebar'])
    .config(config)
    .run(run);
    
  config.$inject = ['$routeProvider', '$locationProvider'];
  function config($routeProvider) {
    $routeProvider.
      when('/', {
        templateUrl: './templates/home.html',
        controller: 'homeCtrl',
        activetab: 'home'
      }).when('/body', {
        templateUrl: './templates/body.html',
        controller: 'bodyCtrl',
        activetab: 'body'
      }).when('/skin', {
        templateUrl: './templates/skin.html',
        controller: 'skinCtrl',
        activetab: 'skin'
      }).when('/mind', {
        templateUrl: './templates/mind.html',
        controller: 'mindCtrl',
        activetab: 'mind'
      }).when('/login', {
        templateUrl: './templates/login.html',
//        controller: 'LoginController'
      }).when('/register', {
        templateUrl: './templates/register.html',
//        controller: 'RegisterController'
      }).when('/terms', {
        templateUrl: './templates/terms.html'
      }).when('/site-map', {
        templateUrl: './templates/site-map.html'
      });
  }
  
  run.$inject = ['$rootScope', '$location', '$cookieStore', '$http'];
  function run($rootScope, $location, $cookieStore, $http) {
    // Keep user logged in after page refresh
    $rootScope.globals = $cookieStore.get('globals') || {};
    if($rootScope.globals.currentUser) {
      $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
    }
    
  //   $rootScope.$on('$locationChangeStart', function(event, next, current) {
  //     // redirect to login page if not logged in and trying to access a restricted page
  //     var restrictedPage = $.inArray($location.path(), ['/login', '/register']) === -1;
  //     var loggedIn = $rootScope.globals.currentUser;
  //     if (restrictedPage && !loggedIn) {
  //       $location.path('/');
  //     }
  //   });
  }
  
})();