'use strict';

// Directives

angular.module('solaseDirectives', ['solaseServices'])
  .directive('topNav', topNav)
  .directive('foot', foot)
  .directive('scroll', scroll);

function topNav() {
  return {
    restrict: 'E',
    templateUrl: 'templates/_nav.html'
  };
}

function foot() {
  return {
    restrict: 'E',
    templateUrl: 'templates/_footer.html'
  };
}

function scroll($window) {
  return function(scope, element, attrs) {
    angular.element($window).bind("scroll", function() {
      if (this.pageYOffset >= 640) {
        scope.boolChangeClass = true;
      } else {
        scope.boolChangeClass = false;
      }
      scope.$apply();
    });
  };
}