'use strict';

// Controllers

angular.module('solaseControllers', ['angularModalService', 'solaseServices', 'angularify.semantic.sidebar'])
  .controller('homeCtrl', homeCtrl)
  .controller('bodyCtrl', bodyCtrl)
  .controller('mindCtrl', mindCtrl)
  .controller('skinCtrl', skinCtrl)
  .controller('RootCtrl', RootCtrl)
  .controller('ModalController', ModalController)
  .controller('LoginController', LoginController)
  .controller('RegisterController', RegisterController);
  
homeCtrl.$inject = ['$scope', '$route'];
bodyCtrl.$inject = ['$scope', '$route'];
mindCtrl.$inject = ['$scope', '$route'];
skinCtrl.$inject = ['$scope', '$route'];
LoginController.$inject = ['$location', 'AuthenticationService', 'FlashService'];
RegisterController.$inject = ['UserService', '$location', '$rootScope', 'FlashService'];

function homeCtrl($scope, $route) {
  $scope.$route = $route;
}

function bodyCtrl($scope, $route) {
  $scope.$route = $route;
}

function mindCtrl($scope, $route) {
  $scope.tab = 1;
  $scope.$route = $route;
}

function skinCtrl($scope, $route) {
  $scope.$route = $route;
  $scope.tab_a = 1;
  $scope.tab_b = 1;
  $scope.tab_c = 1;
  $scope.data = {
    duraation: null,
    waxing: null
  };
}

function RootCtrl($scope, $route, $timeout, $log, $window, ModalService) {
  $scope.$route = $route;
  $scope.isOpen = false;
  $scope.modalShown = false;
  $scope.toggleModal = function() {
    $scope.modalShown = !$scope.modalShown;
  };
  
  $scope.show = function() {
    ModalService.showModal({
      templateUrl: 'templates/_modal.html',
      controller: 'ModalController'
    }).then(function(modal) {
      modal.element.modal('show');
      modal.close;
    });
  };
  
  $scope.sidebarShow = function() {
    var e = sidebarService.getElem();
    console.log(e)
  };
}

function ModalController($scope, close) {
  $scope.close = function(result) {
    close(result, 500);
  };
}

function LoginController($location, AuthenticationService, FlashService) {
  var vm = this;
  
  vm.login = login;
  
  (function initController() {
    // reset login status
    AuthenticationService.ClearCredentials();
  })();
  
  function login() {
    vm.dataLoading = true;
    AuthenticationService.Login(vm.username, vm.password, function (response) {
      if (response.success) {
        AuthenticationService.SetCredentials(vm.username, vm.password);
        $location.path('/');
      } else {
        FlashService.Error(response.message);
        vm.dataLoading = false;
      }
    });
  }
}

function RegisterController(UserService, $location, $rootScope, FlashService) {
  var vm = this;
  
  vm.register = register;
  
  function register() {
    vm.dataLoading = true;
    UserService.Create(vm.user)
      .then(function (response) {
        if (response.success) {
          FlashService.Success('Registration successful', true);
          $location.path('/login');
        } else {
          FlashService.Error(response.message);
          vm.dataLoading = false;
        }
      });
  }
}